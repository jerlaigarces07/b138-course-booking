const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://dbjerliegarces:YMy5dsXKwwDDdWQn@wdc028-course-booking.pqblj.mongodb.net/b138_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);

mongoose.connection.once('open', () => console.log('Now connected to MongoDBAtlas'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "users" strings to be included for all user routes defind in the "user" route file
app.use("/users", userRoutes);

// Defines the "/courses" string to be included for all course routes defined in the "course" route
app.use("/courses", courseRoutes);

// Listening port
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});